﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HyperlinkBehaviour : MonoBehaviour
{
    [HideInInspector]
    public string URL;

    [HideInInspector]
    public string title;

    Text _text;

    // Use this for initialization
    void Awake () {
        _text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void OnEnable () {
        StartCoroutine(SetName());
    }

    IEnumerator SetName()
    {
        while (string.IsNullOrEmpty(title))
        {
            yield return new WaitForEndOfFrame();
        }
        _text.text = title;
    }

    public void GotoURL()
    {
        if (!string.IsNullOrEmpty(URL)) Application.OpenURL(URL);
    }
}
