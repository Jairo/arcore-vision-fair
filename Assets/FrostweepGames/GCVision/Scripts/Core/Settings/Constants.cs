﻿namespace FrostweepGames.Plugins.GoogleCloud.Vision
{
    public class Constants
    {
        public const string IMAGES_ANNOTATE_REQUEST_URL = "https://vision.googleapis.com/v1/images:annotate";
        public const string API_KEY_PARAM = "?key=";


        public const string GC_API_KEY = ""; // Google Cloud API Key. Only for test! Use your own API Key in Live!
    }
}