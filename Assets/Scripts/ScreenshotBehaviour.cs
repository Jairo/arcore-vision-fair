﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.UI;
using FrostweepGames.Plugins.GoogleCloud.Vision;
using Newtonsoft.Json;

public class ScreenshotBehaviour : MonoBehaviour
{
    private readonly List<string> carMakers = new List<string> {
        "AMC",
        "Acura",
        "Alfa Romeo",
        "Anadol",
        "Aston Martin",
        "Audi",
        "Avanti",
        "BMW",
        "Bentley",
        "Buick",
        "Cadillac",
        "Chevrolet",
        "Chrysler",
        "Dacia",
        "Daewoo",
        "Daihatsu",
        "Datsun",
        "DeLorean",
        "Dodge",
        "Eagle",
        "FIAT",
        "Ferrari",
        "Fisker",
        "Ford",
        "Freightliner",
        "GMC",
        "Geely",
        "Geo",
        "HUMMER",
        "Honda",
        "Hyundai",
        "Infiniti",
        "Isuzu",
        "Jaguar",
        "Jeep",
        "Kia",
        "Lamborghini",
        "Lancia",
        "Land Rover",
        "Lexus",
        "Lincoln",
        "Lotus",
        "MG",
        "MINI",
        "Maserati",
        "Maybach",
        "Mazda",
        "McLaren",
        "Mercedes-Benz",
        "Mercury",
        "Merkur",
        "Mitsubishi",
        "Nissan",
        "Oldsmobile",
        "Opel",
        "Peugeot",
        "Plymouth",
        "Pontiac",
        "Porsche",
        "Proton",
        "RAM",
        "Renault",
        "Rolls-Royce",
        "Rover",
        "SRT",
        "Saab",
        "Saturn",
        "Scion",
        "Skoda",
        "Sterling",
        "Subaru",
        "Suzuki",
        "Tesla",
        "Tofaş",
        "Toyota",
        "Triumph",
        "Volkswagen",
        "Volvo",
        "Yugo",
        "smart"
    };

    // Trigger screenshot
    [HideInInspector]
    public bool captureScreenshot = false;

    // Events
    public UnityEvent onPreScreenshot, onPostScreenshot, onAnnotateSuccess, onAnnotateFailed;

    // Main State Machine
    public Animator stateMachine;

    // Components
    Camera _camera;

    Texture2D texture;
    GCVision GoogleCloudVisionAPI;
    MarketcheckResult result;
    string maker, model;

    public MarketcheckResult Result { get { return result; } }

    private void Start()
    {
        _camera = GetComponent<Camera>();
        texture = new Texture2D(0, 0);
        result = new MarketcheckResult();

        GoogleCloudVisionAPI = GCVision.Instance;
        GoogleCloudVisionAPI.AnnotateFailedEvent += AnnotateFailedEventHandler;
        GoogleCloudVisionAPI.AnnotateSuccessEvent += AnnotateSuccessEventHandler;
    }

    private void Update()
    {
        if (captureScreenshot)
        {
            captureScreenshot = false;
            StartCoroutine(TakeScreenshot());
        }
        // Back button on Android
        else if (Input.GetKey(KeyCode.Escape))
        {
            if (stateMachine.GetInteger("step") != 0)
            {
                stateMachine.SetTrigger("prevStep");
            }
        }
    }

    private void OnDestroy()
    {
        GoogleCloudVisionAPI.AnnotateSuccessEvent -= AnnotateSuccessEventHandler;
        GoogleCloudVisionAPI.AnnotateFailedEvent -= AnnotateFailedEventHandler;
    }

    IEnumerator TakeScreenshot()
    {
        yield return new WaitForEndOfFrame();

        onPreScreenshot.Invoke();

        // create render texture
        RenderTexture rt = new RenderTexture(Screen.width, Screen.height, 24, RenderTextureFormat.ARGB32);
        rt.useMipMap = false;
        rt.antiAliasing = 1;

        // render
        RenderTexture oldRT = RenderTexture.active;
        RenderTexture.active = rt;
        _camera.targetTexture = rt;
        _camera.Render();

        texture = DumpRenderTexture(rt);

        // clean
        _camera.targetTexture = null;
        RenderTexture.active = oldRT;

        onPostScreenshot.Invoke();

        stateMachine.SetTrigger("nextStep");
    }

    IEnumerator RequestMarketCheckEndpoint(string maker, string model)
    {
        string endpoint = "https://marketcheck-prod.apigee.net/v1/search?api_key=HBAb9g5LLgvoK24R1yA3MkyTILFBtkyt&make=" + maker.ToLower() + "&model=" + model.ToLower();
        using (UnityWebRequest www = UnityWebRequest.Get(endpoint))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                // TODO Handle Error!
                yield break;
            }


            result = JsonConvert.DeserializeObject<MarketcheckResult>(www.downloadHandler.text);
            Debug.Log("ScreenshotBehaviour::RequestMarketCheckEndpoint - marketcheckResults.Count: " + result.num_found);

            stateMachine.SetBool("Has Results", result.num_found > 0);
            stateMachine.SetTrigger("nextStep");
        }
    }

    private Texture2D DumpRenderTexture(RenderTexture rt)
    {
        Texture2D tex = new Texture2D(rt.width, rt.height);
        tex.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
        tex.Apply();
        return tex;
    }

    private void AnnotateFailedEventHandler(string error, long requestIndex)
    {
        onAnnotateFailed.Invoke();
    }

    private void AnnotateSuccessEventHandler(VisionResponse obj, long requestIndex)
    {
        // Check for "%maker% %model" string in results
        model = maker = "";
        foreach (AnnotateImageResponse response in obj.responses)
        {
            if (model != "") break;
            foreach (var entity in response.webDetection.webEntities)
            {
                if (entity.description != null && entity.description.Contains(" "))
                {
                    string[] info = entity.description.Split(' ');
                    maker = info[0];
                    if (carMakers.Contains(maker))
                    {
                        model = info[1];
                        Debug.Log("ScreenshotBehaviour::AnnotateSuccessEventHandler - " + entity.description);
                        break;
                    }
                }
            }
        }

        stateMachine.SetBool("Car Found", !string.IsNullOrEmpty(model));
        stateMachine.SetTrigger("nextStep");
        onAnnotateSuccess.Invoke();
    }

    public void CaptureScreenshot()
    {
        captureScreenshot = true;
    }

    public void AnnotateImage(Texture2D texture)
    {
        if (texture == null) texture = this.texture;

        List<Feature> features = new List<Feature>();
        features.Add(new Feature() { maxResults = 10, type = Enumerators.FeatureType.WEB_DETECTION });
        GoogleCloudVisionAPI.Annotate(new List<AnnotateRequest>()
        {
            new AnnotateRequest()
            {
                features = features,
                image = new FrostweepGames.Plugins.GoogleCloud.Vision.Image()
                {
                    content = Convert.ToBase64String(texture.EncodeToPNG())
                }
            }
        });
    }

    public void CopyCarInfoTo (Text dest)
    {
        dest.text = maker + " " + model;
    }

    public void SearchInMarketCheck()
    {
        StartCoroutine(RequestMarketCheckEndpoint(maker, model));
    }
}

// Marketcheck Class Helpers

public class Media
{
    public List<string> photo_links { get; set; }
}

public class Dealer
{
    public int id { get; set; }
    public string website { get; set; }
    public string name { get; set; }
    public string dealer_type { get; set; }
    public string street { get; set; }
    public string city { get; set; }
    public string state { get; set; }
    public string country { get; set; }
    public string latitude { get; set; }
    public string longitude { get; set; }
    public string zip { get; set; }
    public string phone { get; set; }
}

public class Build
{
    public int year { get; set; }
    public string make { get; set; }
    public string model { get; set; }
    public string body_type { get; set; }
    public string fuel_type { get; set; }
    public double engine_size { get; set; }
    public string engine_block { get; set; }
    public int doors { get; set; }
    public int cylinders { get; set; }
    public string made_in { get; set; }
    public string trim_r { get; set; }
    public string trim { get; set; }
    public string vehicle_type { get; set; }
    public string transmission { get; set; }
    public string drivetrain { get; set; }
    public string engine { get; set; }
    public string steering_type { get; set; }
    public string antibrake_sys { get; set; }
    public string tank_size { get; set; }
    public string overall_height { get; set; }
    public string overall_length { get; set; }
    public string overall_width { get; set; }
    public string std_seating { get; set; }
    public string highway_miles { get; set; }
    public string city_miles { get; set; }
}

public class FinancingOption
{
    public int loan_term { get; set; }
    public double loan_apr { get; set; }
    public int estimated_monthly_payment { get; set; }
    public int? down_payment_percentage { get; set; }
}

public class MarketcheckCar
{
    public string id { get; set; }
    public string vin { get; set; }
    public string heading { get; set; }
    public int price { get; set; }
    public int miles { get; set; }
    public string data_source { get; set; }
    public string vdp_url { get; set; }
    public bool carfax_1_owner { get; set; }
    public bool carfax_clean_title { get; set; }
    public int dom { get; set; }
    public int dom_180 { get; set; }
    public int dom_active { get; set; }
    public string seller_type { get; set; }
    public string inventory_type { get; set; }
    public string stock_no { get; set; }
    public int last_seen_at { get; set; }
    public DateTime last_seen_at_date { get; set; }
    public int scraped_at { get; set; }
    public DateTime scraped_at_date { get; set; }
    public int first_seen_at { get; set; }
    public DateTime first_seen_at_date { get; set; }
    public int ref_price { get; set; }
    public int ref_price_dt { get; set; }
    public int ref_miles { get; set; }
    public int ref_miles_dt { get; set; }
    public string source { get; set; }
    public Media media { get; set; }
    public Dealer dealer { get; set; }
    public Build build { get; set; }
    public string exterior_color { get; set; }
    public string interior_color { get; set; }
    public int? msrp { get; set; }
    public List<FinancingOption> financing_options { get; set; }
}

public class MarketcheckResult
{
    public int num_found { get; set; }
    public List<MarketcheckCar> listings { get; set; }
}