﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateProperties : StateMachineBehaviour
{
    public int index;

    StateMachine stateMachine;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // anyState states should have signed indexes
        stateMachine = (stateMachine) ? stateMachine : animator.gameObject.GetComponent<StateMachine>();
        GameObject[] steps = stateMachine.states;
        int realIndex = index;
        if (index > -1) animator.SetInteger("step", index);
        else realIndex = steps.Length + index;

        StateController controller = steps[realIndex].GetComponent<StateController>();

        steps[realIndex].SetActive(true);
        controller.OnStart.Invoke();
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        stateMachine = (stateMachine) ? stateMachine : animator.gameObject.GetComponent<StateMachine>();
        GameObject[] steps = stateMachine.states;
        int realIndex = (index > -1) ? index : steps.Length + index;
        StateController controller = steps[realIndex].GetComponent<StateController>();

        controller.OnEnd.Invoke();
        steps[realIndex].SetActive(false);
    }
}
