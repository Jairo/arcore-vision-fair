﻿using UnityEngine;
using UnityEngine.UI;

public class PopulateListController : MonoBehaviour
{
    public ScreenshotBehaviour source;

    public GameObject target, item;

    HyperlinkBehaviour _hyperlinkBehaviour;
    Text _text;

    void OnEnable ()
    {
        foreach (MarketcheckCar car in source.Result.listings)
        {
            // Instantiate & setup
            GameObject child = Instantiate(item);
            child.transform.SetParent(target.transform);
            child.transform.localScale = child.transform.localPosition = child.transform.lossyScale;

            HyperlinkBehaviour hyperlinkBehaviour = child.GetComponentInChildren<HyperlinkBehaviour>();
            hyperlinkBehaviour.URL = car.vdp_url;
            hyperlinkBehaviour.title = car.heading;

            child.SetActive(true);
        }
    }
	
	void Start () {

    }
}
